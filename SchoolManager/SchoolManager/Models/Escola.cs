﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManager.Models
{
    
    public class Escola
    {
        public int EscolaID { get; set; }
        public String NomeEscola { get; set; }
        public String RazaoSocial { get; set; }
        public String CPNJ { get; set; }
        public String NomeFantasia { get; set; }
        public String Endereco { get; set; }
        public int NumeroEndereco { get; set; }
        public String Bairro { get; set; }
        public Enumeradores.Estado Estado { get; set; }
        public String Cidade { get; set; }
        public String Telefone { get; set; }
        public String Email { get; set; }
        
      
        

    }
}