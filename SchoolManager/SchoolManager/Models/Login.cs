﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManager.Models
{
    public class Login
    {
        public int LoginID { get; set; }
        public String NomeUsuario { get; set; }
        public String Senha { get; set; }
        public Enumeradores.TipoLogin TipoAcesso { get; set; }
    }
}