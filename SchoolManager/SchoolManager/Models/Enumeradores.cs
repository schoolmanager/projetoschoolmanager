﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManager.Models
{
    public class Enumeradores
    {
        public enum Estado
        {
            AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MT, MS, MG, PA, PB, PR, PE, PI, RJ, RN, RS, RO, RR, SC, SP, SE, TO
        }

        public enum Bim
        {
            Primeiro,
            Segundo,
            Terceiro,
            Quarto
        }

        public enum Sexo
        {
            Masculino,
            Feminino
        }

        public enum TipoLogin
        {
            Escola,
            Secretaria,
            Diretoria,
            Pedagogia,
            Pai,
            Aluno
        }

        public enum Turno
        {
            Manha,
            Tarde,
            Noite,
            Integral
        }

        public enum DiasUteis
        {
            Segunda,
            Terça,
            Quarta,
            Quinta,
            Sexta
        }

        public enum HorarioAula
        {
            Aula1,
            Aula2,
            Aula3,
            Intervalo,
            Aula4,
            Aula5
        }
        public enum Mes
        {
            Janeiro,
            Fevereiro,
            Março,
            Abril,
            Maio,
            Junho,
            Julho,
            Agosto,
            Setembro,
            Outubro,
            Novembro,
            Dezembro
        }
    }
}