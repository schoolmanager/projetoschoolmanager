﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManager.Models
{
    public class Secretaria
    {
        public int SecretariaID { get; set; }
        public String NomeSecretaria { get; set; }
        public String EmailSecretaria { get; set; }
        public int EscolaID { get; set; }
        public Escola Escola { get; set; }
        public int LoginID { get; set; }
        public Login Login { get; set; }

    }
}