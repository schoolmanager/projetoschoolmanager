﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Pagina Inicial";

            return View();
        }
        public ActionResult Sobre()
        {
            ViewBag.Title = "Sobre Nós";
            return View();
        }
        public ActionResult Contato()
        {
            ViewBag.Title = "Contato";
            return View();

        }
    }
}
